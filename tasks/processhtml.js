/*
 * grunt-processhtml
 * https://github.com/dciccale/grunt-processhtml
 *
 * Copyright (c) 2013-2014 Denis Ciccale (@tdecs)
 * Licensed under the MIT license.
 * https://github.com/dciccale/grunt-processhtml/blob/master/LICENSE-MIT
 */

'use strict';

var _ = require('lodash');

module.exports = function (grunt) {

  var HTMLProcessor = require('./htmlprocessor');
  var path = require('path');

  grunt.registerMultiTask('processhtml', 'Process html files at build time to modify them depending on the release environment', function () {
    //this.options 是一个函数?  应该是grunt的某种机制?  可能是传入的options为default, 会用用户在initConfig里传入的设置取代它
    //var userOptions = this.options({});
    var options = this.options({
      process: false,
      data: {},
      templateSettings: null,
      includeBase: null,
      commentMarker: 'build',
      strip: false,
      recursive: false,
      customBlockTypes: [],
      environment: this.target
    });

    console.log('processhtml started');
    //console.log(this);

    var html = new HTMLProcessor(options);

    this.files.forEach(function (f) {
      var src = f.src.filter(function (filepath) {
        if (!grunt.file.exists(filepath)) {
          grunt.log.warn('Source file "' + filepath + '" not found.');
          return false;
        } else {
          return true;
        }
      }).map(function (filepath) {
        var content = html.process(filepath);
        //console.log('after process, content: ', content);

        // NOTE: 重要部分... 经过process以后应该是成了一个lodash.template可以使用的字符串,但是我们需要对data做更灵活的处理
        var data = _.extend(html.data, {
          src: filepath,
          dest: filepath,
          filename: path.basename(filepath),
        });
        _.each(data, function (val, key) {
          if(_.isFunction(val)){
            var r = val.call(html, html, data);
          }
        });

        if (options.process) {
          content = html.template(content, data, options.templateSettings);
        }

        return content;
      }).join(grunt.util.linefeed);

      grunt.file.write(f.dest, src);
      grunt.log.writeln('File "' + f.dest + '" created.');
    });
  });
};
